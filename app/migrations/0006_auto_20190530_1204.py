# Generated by Django 2.2 on 2019-05-30 10:04

import app.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_auto_20190530_1202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='type',
            field=models.CharField(choices=[('Book', app.models.Type('Book')), ('Audio', app.models.Type('Audio')), ('Video', app.models.Type('Video'))], max_length=20),
        ),
    ]
