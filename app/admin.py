from django.contrib import admin
from django.contrib.auth.models import Group, User

from .models import Item

admin.site.unregister(Group)
admin.site.register(Item)
admin.site.unregister(User)
