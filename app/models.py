from django.db import models
import datetime
from enum import Enum

class Type(Enum):
    BOOK ='Book'
    AUDIO = 'Audio'
    VIDEO = 'Video'

class Item(models.Model):
    type = models.CharField(
        max_length = 20,
        choices = [(tag.value, tag.value) for tag in Type]
    )
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200, default='')
    image = models.ImageField(upload_to = 'images/', default = 'images/book.jpg')
    pub_date = models.DateField('date published')
    add_date = models.DateTimeField(('date added to the library'), auto_now=True)

    def __str__(self):
        return self.type + " " + self.title + " by " + self.author
