from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
import datetime

from .models import Item

def detail(request, item_id):
    item = Item.objects.get(pk=item_id)
    template = loader.get_template('app/detail.html')
    context = {
        'item': item,
    }
    return HttpResponse(template.render(context, request))

def index(request):
    try:
        number = int(request.GET['page'])
    except:
        number = 1
    item_list = Item.objects.order_by('-add_date')
    max_number = item_list.count()/5
    item_list = item_list.filter()[5*(number-1):(5*number)]
    template = loader.get_template('app/index.html')
    text = "?page="
    context = {
        'item_list': item_list,
        'number': number,
        'max_number': max_number,
        'text': text,
    }
    return HttpResponse(template.render(context, request))

def search(request):
    template = loader.get_template('app/search.html')
    context = {
    }
    return HttpResponse(template.render(context, request))


@csrf_exempt
def results(request):
    number = int(request.GET['page'])
    item_list = Item.objects.filter(type__contains=request.GET['type'])
    item_list = item_list.filter(title__contains=request.GET['title'])
    item_list = item_list.filter(author__contains=request.GET['author'])
    if(request.GET['year_b'] == ''):
        year_begin = datetime.MINYEAR
    else:
        year_begin = int(request.GET['year_b'])
    if(request.GET['year_e'] == ''):
        year_end = datetime.MAXYEAR
    else:
        year_end = int(request.GET['year_e'])
    if(year_begin >= year_end):
        year_begin,year_end = year_end,year_begin
    item_list = item_list.filter(pub_date__gt=datetime.date(year_begin, 1, 1))
    item_list = item_list.filter(pub_date__lt=datetime.date(year_end, 12, 31))
    max_number = item_list.count()/5
    item_list = item_list.filter()[5*(number-1):(5*number)]
    template = loader.get_template('app/index.html')
    text = "!?type=" + request.GET['type'] + "&title=" + request.GET['title'] + "&author=" + "&year_b=" + request.GET['year_b'] + "&year_e=" + request.GET['year_e'] + request.GET['author'] + "&page="
    context = {
        'item_list': item_list,
        'number': number,
        'max_number': max_number,
        'text': text,
    }
    return HttpResponse(template.render(context, request))
