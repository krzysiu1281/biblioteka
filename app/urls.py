from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    # /app/
	path('', views.index, name='index'),
    path('detail/<int:item_id>/', views.detail, name='detail'),
    path('search/', views.search, name='search'),
	path('!', views.results, name='results'),
]

if settings.DEBUG:
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)